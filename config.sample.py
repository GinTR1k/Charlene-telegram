# Telegram token
token = ''
# Administators' id
admins = []

# File with temporary changes
tmp_file = 'http://www.fa.ru/org/spo/kip/Documents/raspisanie/%D0%98%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D1%8F%20%D0%B2%20%D1%80%D0%B0%D1%81%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B8.pdf'
# File with permanent shedule
perm_file = 'http://www.fa.ru/org/spo/kip/Documents/raspisanie/3%20%D0%BA%D1%83%D1%80%D1%81.pdf'
# Header that we searching for
http_header = 'Last-Modified'
# Human datetime format
human_datetime_format = '%H:%M:%S %d.%m.%Y'

# If needed, use fill this link or make it empty
# usage: socks5://login:password@ip:port
proxy_link = ''

webhook = {
    # True - use webhook, False - use longpoll
    'enabled': False,
    # IP/Host where the bot is running
    'host': '<ip/host where the bot is running>',
    # 443, 80, 88 or 8443 (port need to be 'open')
    'port': 8443,
    # In some VPS you may need to put here the IP addr
    'listen': '0.0.0.0',

    'url_base': 'https://host:port',
    'url_path': '/telebot/',

    # True - if self signed cert via openssl
    'selfsigned_cert': True,
    # Path to the ssl certificate
    'ssl_cert': './webhook_cert.pem',
    # Path to the ssl private key
    'ssl_key': './webhook_pkey.pem'
}
